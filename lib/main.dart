import 'dart:developer';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'FeatureList.dart';

String username = 'A';
String password = '*********';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyA68GiDjbkBCEuAoeLhg9Y_wSWIkwxUxNA",
          authDomain: "samsodien-module-5.firebaseapp.com",
          projectId: "samsodien-module-5",
          storageBucket: "samsodien-module-5.appspot.com",
          messagingSenderId: "373426256095",
          appId: "1:373426256095:web:e35e59d323a4a7e07d916a",
          measurementId: "G-EJMJ6B3MNJ"));
  runApp(MaterialApp(
      home: AnimatedSplashScreen(
          duration: 1500,
          splash: Icons.home,
          nextScreen: Home(),
          splashTransition: SplashTransition.fadeTransition,
          backgroundColor: Colors.lightGreen),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.lightGreen,
        ).copyWith(
          secondary: Colors.green,
        ),
        textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.black)),
      ),
      routes: {
        'home': (context) => Home(),
        'register': (context) => Register(),
        'dashboard': (context) => Dashboard(),
        'Stuff': (context) => Feature1(),
        'Stuff2': (context) => Feature2(),
        'Stuff3': (context) => Feature3(),
        'list': (context) => FeatureList(),
        'EditProfile': (context) => UserProfile(),
      }));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login page'),
        centerTitle: true,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            Container(
                padding: const EdgeInsets.all(10.0),
                child: const Text('Please enter your login details:')),
            Container(
                width: 300.0,
                padding: const EdgeInsets.all(10.0),
                child: const TextField(
                    decoration: InputDecoration(hintText: 'Username'))),
            Container(
                width: 300.0,
                padding: const EdgeInsets.all(10.0),
                child: const TextField(
                    obscureText: true,
                    decoration: InputDecoration(hintText: 'Password'))),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'dashboard');
                    },
                    child: const Text('Sign in'))),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'register');
                    },
                    child: const Text('Register'))),
          ])),
    );
  }
}

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: const Text('Please create a username and password:')),
              Container(
                  width: 300.0,
                  padding: const EdgeInsets.all(10.0),
                  child: const TextField(
                      decoration: InputDecoration(hintText: 'Username'))),
              Container(
                  width: 300.0,
                  padding: const EdgeInsets.all(10.0),
                  child: const TextField(
                      obscureText: true,
                      decoration: InputDecoration(hintText: 'Password'))),
              Container(
                  width: 300.0,
                  padding: const EdgeInsets.all(10.0),
                  child: const TextField(
                      obscureText: true,
                      decoration:
                          InputDecoration(hintText: 'Confirm Password'))),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'home');
                      },
                      child: const Text('Create account'))),
            ]),
      ),
    );
  }
}

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home"),
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
            Container(
                padding: const EdgeInsets.all(10.0),
                child: const Text('Welcome, this is your dashboard.')),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: const Text(
                    'You can edit your profile by clicking the button in the bottom right.')),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: const Text(
                    'Please click one of the button below to view the feature pages:')),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Stuff');
                    },
                    child: const Text('Click if you are a cat person'))),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Stuff2');
                    },
                    child: const Text('Click if you are a dog person'))),
            Container(
                padding: const EdgeInsets.all(10.0),
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Stuff3');
                    },
                    child: const Text(
                        'Click here to request a new feature page'))),
          ])),
      floatingActionButton: FloatingActionButton(
        child: const Text('Edit'),
        onPressed: () {
          Navigator.pushNamed(context, 'EditProfile');
        },
      ),
    );
  }
}

class UserProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit profile"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: const Text('Profile details:')),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: Text('Username: $username')),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: Text('Password: $password')),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'dashboard');
                      },
                      child: const Text('Confirm details'))),
            ]),
      ),
    );
  }
}

class Feature1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cat"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: const Image(
                    image: AssetImage('assets/cat.jpg'),
                    width: 500,
                    height: 500,
                  )),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: const Text(
                      'This is Peanut, Peanut is a cat, Peanut likes lazing about. Here is a rare case of Peanut not doing so.')),
            ]),
      ),
    );
  }
}

class Feature2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dog"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: const Image(
                    image: AssetImage('assets/dog.jpg'),
                    width: 500,
                    height: 500,
                  )),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: const Text(
                      'This is Rolo. Rolos loves the outdoors, Rolo also likes smelling everything. Rolo doesn\'t bite so go ahead and pet him.')),
            ]),
      ),
    );
  }
}

class Feature3 extends StatefulWidget {
  @override
  State<Feature3> createState() => MakeFeature3();
}

class MakeFeature3 extends State<Feature3> {
  @override
  Widget build(BuildContext context) {
    TextEditingController pageNameCont = TextEditingController();
    TextEditingController pageDescCont = TextEditingController();
    TextEditingController pageImgCont = TextEditingController();

    Future MakeFeature3() {
      final pageName = pageNameCont.text;
      final pageDesc = pageDescCont.text;
      final pageImg = pageImgCont.text;

      final ref = FirebaseFirestore.instance.collection("MakeFeature3").doc();
      return ref
          .set({
            "Name": pageName,
            "Description": pageDesc,
            "ImageDescription": pageImg
          })
          .then((value) => log("collection added"))
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature request"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 300.0,
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                    controller: pageNameCont,
                    decoration: InputDecoration(hintText: 'Name of new page')),
              ),
              Container(
                width: 300.0,
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                    controller: pageDescCont,
                    decoration: InputDecoration(
                        hintText: 'Description of main feature')),
              ),
              Container(
                width: 300.0,
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                    controller: pageImgCont,
                    decoration:
                        InputDecoration(hintText: 'Add description for image')),
              ),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        MakeFeature3();
                      },
                      child: const Text('Create page request'))),
              Container(
                  padding: const EdgeInsets.all(10.0),
                  child: ElevatedButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'dashboard');
                      },
                      child: const Text('return to dashboard'))),
              FeatureList(),
            ]),
      ),
    );
  }
}
