import 'dart:developer';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class FeatureList extends StatefulWidget {
  @override
  State<FeatureList> createState() => Features();
}

class Features extends State<FeatureList> {
  TextEditingController editName = TextEditingController();
  TextEditingController editDesc = TextEditingController();
  TextEditingController editImgDesc = TextEditingController();
  final Stream<QuerySnapshot> myFeatures =
      FirebaseFirestore.instance.collection("MakeFeature3").snapshots();
  @override
  Widget build(BuildContext context) {
    void _delete_(name) {
      FirebaseFirestore.instance
          .collection("MakeFeature3")
          .where("Name", isEqualTo: name)
          .get()
          .then((value) {
        value.docs.forEach((element) {
          FirebaseFirestore.instance
              .collection("MakeFeature3")
              .doc(element.id)
              .delete()
              .then((doc) => (print('deleted')));
        });
      });
    }

    void _update_(name) {
      var collection = FirebaseFirestore.instance.collection("MakeFeature3");
      var map = {
        "Name": editName.text,
        "Description": editDesc.text,
        "ImgDescription": editImgDesc.text,
      };
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Update"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: editName,
                  ),
                  TextField(
                    controller: editDesc,
                  ),
                  TextField(
                    controller: editImgDesc,
                  ),
                  TextButton(
                      onPressed: () {
                        FirebaseFirestore.instance
                            .collection("MakeFeature3")
                            .where("Name", isEqualTo: name)
                            .get()
                            .then((value) {
                          value.docs.forEach((element) {
                            FirebaseFirestore.instance
                                .collection("MakeFeature3")
                                .doc(element.id)
                                .update(map)
                                .then((doc) => (print('updated')));
                          });
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"))
                ]),
              ));
    }

    return StreamBuilder(
        stream: myFeatures,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return Text("Something has gone wrong");
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          }
          if (snapshot.hasData) {
            return Row(
              children: [
                Expanded(
                    child: LimitedBox(
                        maxHeight: 200,
                        maxWidth: MediaQuery.of(context).size.width,
                        child: ListView(
                            children: snapshot.data!.docs
                                .map((DocumentSnapshot document) {
                          Map<String, dynamic> data =
                              document.data()! as Map<String, dynamic>;
                          return Column(children: [
                            Card(
                                child: Column(
                              children: [
                                ListTile(
                                  title: Text(data['Name']),
                                  subtitle: Text(data['Description']),
                                ),
                                ButtonTheme(
                                    child: ButtonBar(
                                  children: [
                                    IconButton(
                                      onPressed: () {
                                        _update_(data['Name']);
                                      },
                                      icon: Icon(Icons.edit),
                                    ),
                                    IconButton(
                                        onPressed: () {
                                          _delete_(data['Name']);
                                        },
                                        icon: Icon(
                                          Icons.remove,
                                        ))
                                  ],
                                ))
                              ],
                            ))
                          ]);
                        }).toList()))),
              ],
            );
          } else {
            return Text("no data");
          }
        });
  }
}
